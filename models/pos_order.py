# -*- coding: utf-8 -*-
# Copyright 2020 Multidevas SA
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

from odoo import models


class PosOrder(models.Model):
    _inherit = 'pos.order'

    def _prepare_invoice(self):
        vals = super(PosOrder, self)._prepare_invoice()
        vals.update(team_id=self.session_id.crm_team_id)
        return vals
