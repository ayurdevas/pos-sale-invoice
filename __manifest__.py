# -*- coding: utf-8 -*-
# Copyright 2020 Multidevas SA
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

{
    'name': 'POS Sales Team to Invoice',
    'version': '12.0.0.1',
    'category': 'Sales/Point Of Sale',
    'summary': 'Copy POS Session Sales Team to Invoice',
    'description': 'This module copies the POS Session Sale Team to Invoice',
    'depends': [
        'pos_sale',
    ],
    'data': [],
    'installable': True,
    'author': 'Roberto Sierra',
    'website': 'https://gitlab.com/ayurdevas/pos-sale-invoice',
    'license': 'AGPL-3',
}
